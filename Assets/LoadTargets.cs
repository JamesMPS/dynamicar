﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sample;
using EasyAR;

public class LoadTargets : MonoBehaviour
{
    public GameObject targetPrefab;
    public TargetProperties targetProperties;
    public List<GameObject> targetList;
    public static LoadTargets instance;

    private void Start()
    {
        instance = this;
        RefreshTargets();
       
    }

   public void RefreshTargets()
       {
        for (int i = 0; i < targetList.Count; i++)
            Destroy(targetList[i]);
        targetList.Clear();
        for (int i = 0; i < targetProperties.targets.Length; i++)
        {
            SampleImageTargetBehaviour targetBehaviour;
            ImageTrackerBehaviour tracker = FindObjectOfType<ImageTrackerBehaviour>();
            CreateTarget(targetProperties.targets[i].name, out targetBehaviour);
            targetBehaviour.Bind(tracker);
            if (targetProperties.targets[i].spawnType == "Image")
                targetBehaviour.SetupWithImage(Application.persistentDataPath + "/" + targetProperties.targets[i].name + ".png", StorageType.App, targetProperties.targets[i].name, new Vector2());
            GameObject duck02_1 = Instantiate(Resources.Load("duck02")) as GameObject;
            duck02_1.transform.parent = targetBehaviour.gameObject.transform;
        }
    }

    void CreateTarget(string targetName, out SampleImageTargetBehaviour targetBehaviour)
    {
        GameObject target = new GameObject(targetName);
        target.transform.localPosition = Vector3.zero;
        targetBehaviour = target.AddComponent<SampleImageTargetBehaviour>();
        targetList.Add(target);

    }
}
