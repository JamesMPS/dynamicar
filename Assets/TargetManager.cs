﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using System.IO;
using UnityEngine.SceneManagement;

public class TargetManager : MonoBehaviour
{

    string jsonUrl= "https://mumftp.mpsinteractive.com/optum/ARTargets/targets3.json";
    public TargetProperties targetProperties;
    public GameObject statusPanel;

    public void UpdateTargets()
    {
        statusPanel.SetActive(true);
        StartCoroutine(DownloadTargetRoutine());
    }

    IEnumerator DownloadTargetRoutine()
    {
        WWW w = new WWW(jsonUrl);
        yield return w;
        if(w.error==null)
        {
            string json = w.text;
            Debug.Log("Text-" + json);
            JSONNode root = JSON.Parse(json);
            int currentVersion = PlayerPrefs.GetInt("Version", 0);
            int jsonVersion = root["version"].AsInt;
            //if (jsonVersion>currentVersion)
            //{
                PlayerPrefs.SetInt("Version", jsonVersion);
                PlayerPrefs.Save();

                JSONNode targets = root["targets"];
                Debug.Log("Count " + targets.Count);
                targetProperties.targets = new Target[targets.Count];
                for (int i = 0; i < targets.Count; i++)
                {
                    targetProperties.targets[i].spawnType = targets[i]["spawnType"];
                    targetProperties.targets[i].imageUrl = targets[i]["imageUrl"];
                    targetProperties.targets[i].name = targets[i]["name"];
                    targetProperties.targets[i].size = targets[i]["size"].ReadVector2();
                }

             StartCoroutine(   DownloadImages());
            //}
          
        }

    }

    IEnumerator DownloadImages()
    {
        Debug.Log("DownloadImages " + Application.persistentDataPath);
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < targetProperties.targets.Length; i++)
        {
            Debug.Log("Image URL-" + targetProperties.targets[i].imageUrl);
            WWW w = new WWW(targetProperties.targets[i].imageUrl);
            yield return w;
            if (w.error == null)
            {
                Debug.Log("persistent " + Application.persistentDataPath);
                File.WriteAllBytes(Application.persistentDataPath + "/" + targetProperties.targets[i].name + ".png", w.bytes);
            }
            else
                Debug.LogError("Error downloading image");
        }

        statusPanel.SetActive(false);
        LoadTargets.instance.RefreshTargets();
    }

}
