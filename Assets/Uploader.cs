﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.IO;

public class Uploader : MonoBehaviour
{
     string FTPHost = "ftp://mumftp.mpsinteractive.com/ARTargets";
     string FTPUserName = "optum";
     string FTPPassword = "Mps#12345";
     string FilePath;

    public void UploadFile()
    {
        FilePath = Application.persistentDataPath+ "/idback.jpg";

        
        WebClient client = new System.Net.WebClient();
        //Uri uri = new Uri(FTPHost + new FileInfo(FilePath).Name);
        Uri uri = new Uri(FTPHost + "/" + new FileInfo(FilePath).Name);

        client.UploadProgressChanged += new UploadProgressChangedEventHandler(OnFileUploadProgressChanged);
        client.UploadFileCompleted += new UploadFileCompletedEventHandler(OnFileUploadCompleted);
        client.Credentials = new System.Net.NetworkCredential(FTPUserName, FTPPassword);
        client.UploadFileAsync(uri, "STOR", FilePath);
    }

    void OnFileUploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
    {
        Debug.Log("Uploading Progreess: " + e.ProgressPercentage);
    }

    void OnFileUploadCompleted(object sender, UploadFileCompletedEventArgs e)
    {
        Debug.Log("File Uploaded");
    }

    void Start()
    {
        UploadFile();
    }
}