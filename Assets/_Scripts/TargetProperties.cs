﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="TargetProperty")]
public class TargetProperties : ScriptableObject
{
    public int version;
    public Target[] targets;
}

[System.Serializable]
public struct Target
{
    public string spawnType;
    public string imageUrl;
    public string name;
    public Vector2 size;

}
